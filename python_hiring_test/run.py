"""Main script for generating output.csv."""

import pandas as pd
import numpy as np
import os
import python_hiring_test

RAW_DATA_PATH = os.path.join(python_hiring_test.RAW,'pitchdata.csv')

COMBINATIONS_PATH = os.path.join(python_hiring_test.REFERENCE,'combinations.txt')

OUTPUT_PATH = os.path.join(python_hiring_test.PROCESSED,'output.csv')


def main():
    # Read in raw data and combinations data
    raw_data = pd.read_csv(RAW_DATA_PATH)
    combinations_data = pd.read_csv(COMBINATIONS_PATH)

    # This list holds data frames for each combination
    data_frame_list = np.empty( len(combinations_data.index ), dtype=object) 

    # Iterate through each combination
    for index, combo in combinations_data.iterrows():
        stat = combo.Stat
        subject = combo.Subject
        split = combo.Split

        # Add new data frame to our list
        data_frame_list[index] = create_data_frame_for_combination(raw_data,stat,subject,split)
    
    # Combine all the data frames into one
    final_table = pd.concat(data_frame_list).sort_values(
                    by=['SubjectId','Stat','Split','Subject'])

    # Write this single data frame to csv
    final_table.to_csv(OUTPUT_PATH, index=False) 

'''
    Returns a data frame based on parameters
    Params:
        table: raw data
        stat: AVG, OBP, etc.
        subject: HitterId, PitcherId, etc.
        split: LHH, RHH, etc.

'''
def create_data_frame_for_combination(table, stat, subject, split):
    
    # Restrict the table based on split
    if "RHP" in split:
        table = table[table.PitcherSide == 'R']
    elif "LHP" in split:
        table = table[table.PitcherSide == 'L']
    elif "RHH" in split:
        table = table[table.HitterSide == 'R']
    elif "LHH" in split:
        table = table[table.HitterSide == 'L']
    
    # Sum up the needed data based on the subject
    table = table.groupby(subject).agg({
        "PA": np.sum,
        "AB": np.sum,
        "H": np.sum,
        "TB": np.sum,
        "BB": np.sum,
        "SF": np.sum,
        "HBP": np.sum })

    # Don't include players with less than 25 plate appearances
    table = table[table.PA >= 25]

    # Calculate the statistic we want 
    if stat == 'AVG':
        value = table['H'] / table['AB']
    elif stat == 'OBP':
        value = calc_obp(table)
    elif stat == 'SLG':
        value = calc_slg(table)
    elif stat == 'OPS':
        obp = calc_obp(table)
        slg = calc_slg(table)
        value = obp + slg

    # Create dictionary for the data frame 
    output_data = {'SubjectId': table.index, 
         'Stat': stat, 
         'Split': split,
         'Subject': subject,
         'Value': value.round(3) }

    output_cols = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']

    output_data_frame = pd.DataFrame(data=output_data, columns=output_cols)

    return output_data_frame

'''
    Returns On base percentage
        (Hits + Walks + HBP) / (AB + Walks + HBP + SF)
'''
def calc_obp(table):
    return (table['H'] + table['BB'] + table['HBP']) / (table['AB'] +
                                table['BB'] + table['HBP'] + table['SF'])
'''
    Returns slugging percentage
        Total bases / ABs
'''
def calc_slg(table):
    return table['TB'] / table['AB']


if __name__ == '__main__':
    main()
    
